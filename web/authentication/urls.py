from django.urls import path

from . import views

app_name = 'authentication'
urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.log_into_site, name='log_into_site'),
    path('logout', views.logout_of_site, name='logout_of_site'),
    path('register', views.register, name='register')
]
