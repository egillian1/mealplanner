from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse

import logging
logger = logging.getLogger("docker.console")



def index(request):
    return render(request, 'authentication/login.html')

def log_into_site(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/weekplanner');
    else:
        return render(request, 'authentication/login.html', {'error': 'Login failed, user not found or password incorrect'});

def register(request, error = ""):
    if(request.method == "GET"):
        return render(request, 'authentication/register.html')
    post = request.POST
    username = post['username']
    password = post['password']
    confirm = post['confirm']

    if(password != confirm):
        logger.info('passwords do not match')
        return render(request, 'authentication/register.html', {'error': 'Passwords do not match'})

    if(len(password) < 7):
        logger.info('password too short')
        return render(request, 'authentication/register.html', {'error': 'Password is too short'})

    newUser = User.objects.create_user(username, username, password)
    newUser.save()
    return render(request, 'authentication/login.html', {'notification': 'Successfully registered'})

def logout_of_site(request):
    logout(request)
    return render(request, 'authentication/login.html', {'notification': 'Successfully logged out'})
