from django.db import models

class Tag(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    user = models.CharField(max_length=300, null=False)
    def __str__(self):
        return self.name

class Recipe(models.Model):
    name = models.CharField(max_length=300, null=False, blank=False)
    duration = models.IntegerField()
    url = models.CharField(max_length=400)
    description = models.TextField()
    pub_date = models.DateTimeField('date published')
    user = models.CharField(max_length=300, null=False)
    tags = models.ManyToManyField(Tag)
    def __str__(self):
        return self.name
