from django.urls import path

from . import views

app_name="weekplanner"
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:recipe_id>/edit', views.edit, name='edit'),
    path('<int:recipe_id>/delete', views.delete, name='delete'),
    path('submitchanges/', views.submit_changes, name='submit_changes'),
    path('<int:recipe_id>/', views.detail, name='detail'),
    path('addrecipe/', views.add_recipe, name='add_recipe'),
    path('random/', views.random_recipe, name='random_recipe'),
   
    path('tags/', views.tag_index, name='tag_index'),
    path('addtag/', views.add_tag, name='add_tag'),
    path('submittag/', views.submit_tag, name='submit_tag')
]
