from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from pprint import pprint
import logging, datetime, random
logger = logging.getLogger("docker.console")

from .models import Recipe, Tag

@login_required
def index(request):
    current_user = request.user
    latest_recipe_list = Recipe.objects.filter(user = current_user)
    latest_recipe_list = latest_recipe_list.order_by('name')
    context = {
        'latest_recipe_list': latest_recipe_list,
    }
    return render(request, 'recipes/index.html', context)

@login_required
def detail(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    logger.info("Details")
    logger.info(recipe);
    return render(request, 'recipes/detail.html', {'recipe': recipe})

@login_required
def add_recipe(request):
    logger.info("Adding recipe")
    user_tags = Tag.objects.filter(user=request.user)
    return render(request, 'recipes/add.html', {'tag_list': user_tags})

@login_required
def edit(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    logger.info("Editing recipe")
    logger.info(recipe)
    user_tags = Tag.objects.filter(user=request.user)
    recipe_tags = recipe.tags.all()
    return render(request, 'recipes/edit.html', {'recipe': recipe, 'recipe_tags': recipe_tags, 'tag_list': user_tags})

@login_required
def submit_changes(request):
    logger.info("submitting changes")
    post = request.POST
    recipe_id = post.get('id')
    current_user = request.user
    user_tags = Tag.objects.filter(user=request.user)
    
    if recipe_id is not None:
        logger.info('id found')
        recipe = get_object_or_404(Recipe, pk=recipe_id)
    else:
        logger.info('no id found')
        now = datetime.datetime.now()
        recipe = Recipe(pub_date = now, user = current_user)

    # Remove all tags and then add the ones
    recipe.tags.clear()
    for field in post:
        if 'tag_' in field:
            logger.info(field)
            tag_name = field.replace('tag_', '')
            tag_object = user_tags.filter(name=tag_name)[0]
            recipe.tags.add(tag_object)
    
    recipe.name = post.get('name')
    recipe.duration = post.get('duration')
    recipe.url = post.get('url')
    recipe.description = post.get('description')
    recipe.save()

    return HttpResponseRedirect(reverse('weekplanner:index'))

@login_required
def delete(request, recipe_id):
    logger.info("deleting recipe id")
    logger.info(recipe_id)
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    recipe.delete()
    return HttpResponseRedirect(reverse('weekplanner:index'), {'notification': 'Object successfully deleted'})

@login_required
def random_recipe(request):
    logger.info("random recipe being picked")
    current_user = request.user
    user_recipes = Recipe.objects.filter(user = current_user)
    logger.info(user_recipes)
    random_recipe = random.choice(user_recipes)
    return render(request, 'recipes/detail.html', {'recipe': random_recipe})

# Tag views

@login_required
def tag_index(request):
    current_user = request.user
    user_tags = Tag.objects.filter(user=current_user)
    return render(request, 'tags/index.html', {'tag_list': user_tags})

@login_required
def add_tag(request):
    return render(request, 'tags/add.html')

@login_required
def submit_tag(request):
    post = request.POST
    current_user = request.user
    tag_name = post.get('name')
    tag = Tag(user = current_user, name = tag_name)
    tag.save()
    return HttpResponseRedirect(reverse('weekplanner:index'), {'notification' : 'Tag successfully created'})
